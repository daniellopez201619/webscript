/*
Declarar el Array
*/
let miArreglo = [];
let otroArreglo = [2, 4, 6, 12, 20];

// Acceso a un elemento de array
console.log(otroArreglo[0]);
console.log("El valor es " + otroArreglo[3]);
//Longitud array
console.log(otroArreglo.length);
//Mostrar números aleatorios
let aleatorio = (Math.random() * 100).toFixed(0);
console.log("El número es " + aleatorio);

function pro(otroArreglo) {
    let acumular = 0;
    let promedio = 0;
    for (let i = 0; i < otroArreglo.length; ++i) {
        otroArreglo[i] = (Math.random() * 100).toFixed(0);
        acumular = + otroArreglo[i];
    }
    promedio = acumular / otroArreglo.length;
    return promedio.toFixed(1);

}

let res = pro(otroArreglo);
console.log(res);

//Tarea : Generar un array y llenarlo con valores aleatorios
function llenar(otroArreglo) {
/Tomar el txtCantidad/ 
    const cantidad = document.getElementById('txtCantidad').value;
    for(let i = 0; i < cantidad; ++i){
        otroArreglo[i] = (Math.random()*100).toFixed(0);
    }

}

function mostrar(){
    llenar (otroArreglo);
    numPares(otroArreglo);
    numImpares(otroArreglo);
    document.getElementById("txtSimetrico").value = simetrico(otroArreglo);
 const lista = document.getElementById('lista');
    for(let i = 0; i < otroArreglo.length; ++i){
        valor = otroArreglo[i];
        lista[i] = new Option(valor, 'valor' + valor);
    }
}

function numPares(otroArreglo) {
    const txtPares = document.getElementById('txtPares');
    let pares = 0;
    for(let i=0; i < otroArreglo.length; ++i){
        if(otroArreglo[i]%2 == 0) ++pares;

    }

    txtPares.value = pares;

}

function numImpares(otroArreglo){
    let impar = 0;
    for(let i=0; i < otroArreglo.length; ++i){
        if(otroArreglo[i]%2!== 0) ++impar;

    }

    document.getElementById('txtImpares').value = impar;

}


function simetrico(otroArreglo) {
    let numPares = otroArreglo.filter(x => x % 2 === 0).length;
    let numImpares = otroArreglo.filter(x => x % 2 !== 0).length;
    return numPares === numImpares ? "El arreglo es simétrico." : "El arreglo es asimétrico.";

}

document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("txtSimetrico").value = ""; 
});

function limpiar() {
    document.getElementById("txtCantidad").value = "";
    document.getElementById("lista").innerHTML = "";
    document.getElementById("txtPares").value = "";
    document.getElementById("txtImpares").value = "";
    document.getElementById("txtSimetrico").value = "";
}

